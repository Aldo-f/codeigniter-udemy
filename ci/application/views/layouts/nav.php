<?php
//method 1
$active = "active";
$home = "";
$projects = "";
$register = "";

if ($this->uri->segment(1) == 'projects') {
    $projects = $active;
} else if ($this->uri->segment(2) == 'register') {
    $register = $active;
} else {
    $home = $active;
}

//method 2


?>


<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php base_url() ?>/">CI App</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="<?= $home ?>"><a href="<?php base_url() ?>/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="<?= $projects ?>"><a href="<?php base_url() ?>/projects">Projects</a></li>
                <li class="<?= $register ?>"><a href="<?php base_url() ?>/users/register">Register</a></li>
            </ul>
            <?php if ($this->session->userdata('logged_id')) : ?>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php base_url() ?>/users/logout">Logout</a></li>

                <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                            aria-expanded="false">Dropdown <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                        </ul>
                    </li> -->
            </ul>
            <?php endif; ?>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
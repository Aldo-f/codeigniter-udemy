<div class="row">
    <div class="col-sm-8 col-xs-12">
        <h1><?php echo $task->task_name ?></h1>
        <p>Project Name: <?php echo $project_name ?></p>
        <p>Created: <?php echo $task->date_created ?></p>
        <p>Due date: <?php echo $task->due_date ?></p>
        <h4>Description</h4>
        <p class="task-description"><?php echo $task->task_body ?></p>
    </div>
    <div class="col-sm-4 col-xs-12 pull-right">
        <ul class="list-group">
            <h3>Project Actions</h3>
            <li class="list-group-item"> <a href="<?php base_url() ?>/tasks/edit/<?php echo $task->id ?>">Edit</a>
            </li>
            <li class="list-group-item"> <a
                    href="<?php base_url() ?>/tasks/delete/<?php echo $task->project_id ?>/<?php echo $task->id ?>">Delete</a>
            </li>
            <li class="list-group-item"><a href="<?php base_url() ?>/tasks/mark_complete/<?php echo $task->id ?>">Mark
                    complete</a></li>
            <li class="list-group-item"> <a
                    href="<?php base_url() ?>/tasks/mark_incomplete/<?php echo $task->id ?>">Mark
                    incomplete</a></li>

        </ul>
    </div>
</div>
<p class="bg-success">
    <?php if ($this->session->flashdata('login_success')) : ?>
    <?php echo $this->session->flashdata('login_success'); ?>
    <?php endif; ?>
</p>
<p class="bg-danger">
    <?php if ($this->session->flashdata('login_failed')) : ?>
    <?php echo $this->session->flashdata('login_failed'); ?>
    <?php endif; ?>

</p>

<p class="bg-success">
    <?php if ($this->session->flashdata('user_register')) : ?>
    <?php echo $this->session->flashdata('user_register'); ?>
    <?php endif; ?>
</p>

<p class="bg-danger">
    <?php if ($this->session->flashdata('no_access')) : ?>
    <?php echo $this->session->flashdata('no_access'); ?>
    <?php endif; ?>

</p>

<div class="jumbotron">
    <h1 class="text-center">Welcome to the CI App</h1>
</div>

<?php if (isset($projects)) : ?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3>Projects</h3>
    </div>
    <div class="panel-body">
        Info about projects
    </div>
    <ul class="list-group">
        <?php foreach ($projects as $project) : ?>
        <li class="list-group-item">
            <a href="<?php base_url() ?>/projects/display/<?php echo $project->id ?>" ?>
                <?php echo $project->project_name ?>
                <!-- <?php echo $project->project_body ?> -->
            </a>
        </li>

        <?php endforeach; ?>
    </ul>
</div>




<?php endif; ?>

<?php if (isset($tasks)) : ?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3>Tasks</h3>
    </div>
    <div class="panel-body">
        Info about tasks
    </div>
    <ul class="list-group">
        <?php foreach ($tasks as $task) : ?>
        <li class="list-group-item">
            <a href="<?php base_url() ?>/tasks/display/<?php echo $task->id ?>">
                <?php echo $task->task_name ?>
                <!-- <?php echo $task->task_body ?> -->
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?>
<?php

class Project_model extends CI_Model
{
    public function get_project($id)
    {
        return $this->db->where([
            'id' => $id
        ])
            ->get('projects')->row();
    }

    public function get_projects()
    {
        // return $this->db->get('projects');

        $query = $this->db->get('projects');

        return $query->result();
    }

    public function get_all_projects($user_id)
    {
        return $this->db->where('project_user_id', $user_id)->get('projects')->result();
    }

    public function create_project($data)
    {
        $insert_query = $this->db->insert('projects', $data);

        return $insert_query;
    }

    public function edit_project($project_id, $data)
    {
        $this->db->where([
            'id' => $project_id
        ]);
        $this->db->update('projects', $data);

        return true;
    }

    public function get_projects_info($project_id)
    {
        return $this->db->where(['id' => $project_id])->get('projects')->row();

        // return $this->db->where('id',  $project_id)->get('projects')->row();

        // $this->db->where('id', $project_id);
        // return $this->db->get('projects')->row();
    }

    public function get_project_task($project_id, $active = true)
    {

        $this->db
            ->select('
                tasks.task_name, 
                tasks.task_body, 
                tasks.id as task_id, 
                projects.project_name, 
                projects.project_body 
            ')
            ->from('tasks')
            ->join('projects', 'projects.id = tasks.project_id')
            ->where('tasks.project_id', $project_id);

        if ($active == true) {
            $this->db->where('tasks.status', 0);
        } else {
            $this->db->where('tasks.status', 1);
        }

        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return false;
        }
        return $query->result();
    }

    public function delete_project($project_id)
    {
        $this->db->where('id', $project_id)->delete('projects');
    }

    public function delete_project_tasks($project_id)
    {
        return $this->db->where('project_id', $project_id)->delete('tasks');
    }
}
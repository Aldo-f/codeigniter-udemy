<?php
class Task_model extends CI_Model
{
    public function get_task($task_id)
    {
        return $this->db->where('id', $task_id)->get('tasks')->row();
    }

    public function get_all_tasks($user_id)
    {
        return $this->db->where('project_user_id', $user_id)
            ->join('tasks', 'projects.id = tasks.project_id')
            ->get('projects')
            ->result();
    }

    public function create_task($data)
    {
        $query = $this->db->insert('tasks', $data);
        return $query;
    }

    public function get_task_project_id($task_id)
    {
        return $this->db->where('id', $task_id)
            ->get('tasks')->row()
            ->project_id;
    }

    public function get_task_project_data($task_id)
    {
        return $this->db->where('id', $task_id)
            ->get('tasks')->row();
    }

    public function get_project_name($project_id)
    {
        return $this->db->where('id', $project_id)
            ->get('projects')->row()
            ->project_name;
    }

    public function edit_task($task_id, $data)
    {
        $this->db->where(['id' => $task_id])
            ->update('tasks', $data);

        return true;
    }

    public function delete_task($task_id)
    {
        $this->db->where('id', $task_id)->delete('tasks');
    }

    public function mark_task_complete($task_id)
    {
        $this->db->set('status', 1)
            ->where('id', $task_id)
            ->update('tasks');

        return true;
    }

    public function mark_task_incomplete($task_id)
    {
        $this->db->set('status', 0)
            ->where('id', $task_id)
            ->update('tasks');

        return true;
    }
}
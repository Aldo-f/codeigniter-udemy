<?php



class Users extends CI_Controller
{
    public function show($user_id)
    {
        // $this->load->model('user_model');

        $result = $this->user_model->get_users($user_id, 'rico');

        $data['welcome'] = "Welcome to my page";
        $data['results'] = $result;

        $this->load->view('user_view', $data);

        // var_dump($result);

        // foreach ($result as $object) {
        //     echo $object->id . '</br>';
        //     echo $object->password . '</br>';
        //     echo $object->username . '</br>';
        // }
    }

    public function insert()
    {
        $username = 'peter';
        $password = 'secret';

        $this->user_model->create_users([
            'username' => $username,
            'password' => $password

        ]);
    }

    public function update()
    {
        $id = 3;

        $username = 'William';
        $password = 'notSoSecret';

        $this->user_model->update_users([
            'username' => $username,
            'password' => $password

        ], $id);
    }

    public function delete()
    {
        $id = 3;
        $this->user_model->delete_users($id);
    }

    public function register()
    {

        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $data['main_view'] = 'users/register_view';
            $this->load->view('layouts/main', $data);
        } else {

            if ($this->user_model->create_user()) {
                //created
                $this->session->set_flashdata('user_register', 'User has been register');
                redirect('home/index');
            } else {
                # some errors occurred 
            }
        }
    }

    public function login()
    {

        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]');
        // $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $data = array(
                'errors' => validation_errors()
            );
            $this->session->set_flashdata($data);
            redirect('home'); // TODO: return to the page where you came from
        } else {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $user_id = $this->user_model->login_user($username, $password);

            if ($user_id) {
                $user_data = array(
                    'user_id' => $user_id,
                    'username' => $username,
                    'logged_id' => true
                );
                $this->session->set_userdata($user_data);
                $this->session->set_flashdata('login_success', 'You are now logged in');

                // $data['main_view'] = 'admin_view';
                // $this->load->view('layouts/main', $data);

                redirect('home/index');
            } else {
                $this->session->set_flashdata('login_failed', 'Sorry, you are not logged in');
            }
        };

        // echo $this->input->post('username');
        // echo $_POST['username'];


    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('home/index');
    }
}
<?php

class Tasks extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        // var_dump('inside Tasks');
    }

    public function display($task_id)
    {

        $data['project_id'] = $this->task_model->get_task_project_id($task_id);
        $data['project_name'] = $this->task_model->get_project_name($data['project_id']);

        $data['task'] = $this->task_model->get_task($task_id);

        $data['main_view'] = 'tasks/display';

        $this->load->view('layouts/main', $data);
    }

    public function create($project_id)
    {
        // var_dump('In create');
        $this->form_validation->set_rules('task_name', 'Task Name', 'trim|required');
        $this->form_validation->set_rules('task_body', 'Task Body', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            // load view
            $data['main_view'] = 'tasks/create_task';
            $this->load->view('layouts/main', $data);
        } else {
            //  progress data
            $data = [
                'project_id' => $project_id,
                'task_name' => $this->input->post('task_name'),
                'task_body' => $this->input->post('task_body'),
                'due_date' => $this->input->post('due_date'),
            ];

            if ($this->task_model->create_task($data)) {
                // created
                $this->session->set_flashdata('task_created', 'Your task has been created');

                redirect('projects/index');
            } else {
                // some error
            }
        }
    }

    public function edit($task_id)
    {
        // var_dump('In edit');
        $this->form_validation->set_rules('task_name', 'Task Name', 'trim|required');
        $this->form_validation->set_rules('task_body', 'Task Body', 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            // get some data
            $data['project_id'] = $this->task_model->get_task_project_id($task_id);
            $data['project_name'] = $this->task_model->get_project_name($data['project_id']);
            $data['the_task'] = $this->task_model->get_task_project_data($task_id);

            // load view
            $data['main_view'] = 'tasks/edit_task';
            $this->load->view('layouts/main', $data);
        } else {
            $project_id = $this->task_model->get_task_project_id($task_id);

            //  progress data
            $data = [
                'project_id' => $project_id,
                'task_name' => $this->input->post('task_name'),
                'task_body' => $this->input->post('task_body'),
                'due_date' => $this->input->post('due_date'),
            ];

            if ($this->task_model->edit_task($task_id, $data)) {
                // created
                $this->session->set_flashdata('task_updated', 'Your task has been updated');

                redirect('projects/index');
            } else {
                // some error
            }
        }
    }

    public function delete($project_id, $task_id)
    {
        $this->task_model->delete_task($task_id);
        $this->session->set_flashdata('task_delted', 'Task has been deleted');
        redirect('projects/display/' . $project_id);
    }

    public function mark_complete($task_id)
    {
        if ($this->task_model->mark_task_complete($task_id)) {

            $project_id = $this->task_model->get_task_project_id($task_id);
            $this->session->set_flashdata('mark_done', 'This task has been completed');
            // var_dump($project_id);
            redirect('projects/display/' . $project_id);
        }
    }

    public function mark_incomplete($task_id)
    {
        if ($this->task_model->mark_task_incomplete($task_id)) {

            $project_id = $this->task_model->get_task_project_id($task_id);
            $this->session->set_flashdata('mark_undone', 'This task has been marked undone');
            // var_dump($project_id);
            redirect('projects/display/' . $project_id);
        }
    }
}
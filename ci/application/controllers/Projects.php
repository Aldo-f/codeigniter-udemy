<?php

class Projects extends CI_Controller
{
    // TODO: check admin

    public function __construct()
    {
        parent::__construct();


        // var_dump('inside Projects');

        if (!$this->session->userdata["logged_id"]) {
            $this->session->set_flashdata('no_access', 'Sorry, you are not allowed or not logged in');
            redirect('home/index');
        }
    }

    public function index()
    {
        $user_id = $this->session->userdata('user_id');
        $data['projects'] = $this->project_model->get_all_projects($user_id);
        // $data['projects'] = $this->project_model->get_projects(); // all projects
        $data['main_view'] = 'projects/index';

        $this->load->view('layouts/main', $data);
    }

    public function display($project_id)
    {
        $data['not_completed_tasks'] = $this->project_model->get_project_task($project_id, false);
        $data['completed_tasks'] = $this->project_model->get_project_task($project_id, true);

        $data['project_data'] = $this->project_model->get_project($project_id);

        $data['main_view'] = 'projects/display';
        // $data['project'] = $this->project_model->get_project($this->;);

        $this->load->view('layouts/main', $data);
    }

    public function create()
    {
        $this->form_validation->set_rules('project_name', 'Project Name', 'trim|required');
        $this->form_validation->set_rules('project_body', 'Project Description', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            // load view
            $data['main_view'] = 'projects/create_project';
            $this->load->view('layouts/main', $data);
        } else {
            //  progress data
            $data = [
                'project_user_id' => $this->session->userdata('user_id'),
                'project_name' => $this->input->post('project_name'),
                'project_body' => $this->input->post('project_body'),
            ];

            if ($this->project_model->create_project($data)) {
                // created
                $this->session->set_flashdata('project_created', 'Your Project has been created');

                redirect('projects/index');
            } else {
                // some error
            }
        }
    }

    public function edit($project_id)
    {
        $this->form_validation->set_rules('project_name', 'Project Name', 'trim|required');
        $this->form_validation->set_rules('project_body', 'Project Description', 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            $data['project_data'] = $this->project_model->get_projects_info($project_id);

            // load view
            $data['main_view'] = 'projects/edit_project';
            $this->load->view('layouts/main', $data);
        } else {
            //  progress data
            $data = [
                'project_user_id' => $this->session->userdata('user_id'),
                'project_name' => $this->input->post('project_name'),
                'project_body' => $this->input->post('project_body'),
            ];

            if ($this->project_model->edit_project($project_id, $data)) {
                // created
                $this->session->set_flashdata('project_updated', 'Your Project has been updated');

                redirect('projects/index');
            } else {
                // some error
            }
        }
    }

    public function delete($project_id)
    {
        $this->project_model->delete_project_tasks($project_id);

        $this->project_model->delete_project($project_id);
        $this->session->set_flashdata('project_deleted', 'Project has been deleted');
        redirect('projects/index');
    }
}
-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: database
-- Gegenereerd op: 03 feb 2020 om 20:06
-- Serverversie: 5.7.28
-- PHP-versie: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `errand_db`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `project_user_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_body` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `projects`
--

INSERT INTO `projects` (`id`, `project_user_id`, `project_name`, `project_body`, `date_created`) VALUES
(11, 8, 'PHP courses', 'Some info', '2020-01-17 11:34:20'),
(12, 8, 'JavaScript', 'Some info', '2020-01-17 15:05:55'),
(13, 8, 'My new project', 'Some description', '2020-02-03 17:45:30');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `task_name` varchar(255) NOT NULL,
  `task_body` text NOT NULL,
  `due_date` date DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `tasks`
--

INSERT INTO `tasks` (`id`, `project_id`, `task_name`, `task_body`, `due_date`, `date_created`, `status`) VALUES
(14, 11, 'PHP task', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint debitis pariatur nemo suscipit nihil explicabo placeat praesentium quod perspiciatis temporibus eum, cupiditate natus, odio necessitatibus quo consectetur magnam ipsam inventore.', '0000-00-00', '2020-01-17 15:20:01', 1),
(15, 11, 'qxsc', 'xsc', '0000-00-00', '2020-01-17 15:20:10', 1),
(17, 12, 'Javascript Task', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint debitis pariatur nemo suscipit nihil explicabo placeat praesentium quod perspiciatis temporibus eum, cupiditate natus, odio necessitatibus quo consectetur magnam ipsam inventore.', '2020-02-02', '2020-01-17 15:55:32', 0),
(18, 12, 'Another task for JavaScript', 'xdcfvf', '0000-00-00', '2020-01-17 15:59:00', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `email`, `register_date`) VALUES
(1, 'rico', '$2y$12$86kXcCAN/GHX9Q0i9SSoXOGDhZDtl66rGoJdyDZfnuaexGX/pYJc.', '', '', '', '2020-01-16 08:23:54'),
(2, 'aldo', '$2y$12$86kXcCAN/GHX9Q0i9SSoXOGDhZDtl66rGoJdyDZfnuaexGX/pYJc.', '', '', '', '2020-01-16 08:23:54'),
(4, 'username', '$2y$12$86kXcCAN/GHX9Q0i9SSoXOGDhZDtl66rGoJdyDZfnuaexGX/pYJc.', 'from form', 'last name', 'email', '2020-01-16 09:06:05'),
(5, 'username', '$2y$12$86kXcCAN/GHX9Q0i9SSoXOGDhZDtl66rGoJdyDZfnuaexGX/pYJc.', 'from form', 'last name', 'email', '2020-01-16 09:08:17'),
(6, 'username', '$2y$12$86kXcCAN/GHX9Q0i9SSoXOGDhZDtl66rGoJdyDZfnuaexGX/pYJc.', 'from form', 'last name', 'email', '2020-01-16 09:17:40'),
(7, 'peter20', '$2y$12$86kXcCAN/GHX9Q0i9SSoXOGDhZDtl66rGoJdyDZfnuaexGX/pYJc.', 'peter', 'williams', 'peter012@gmail.com', '2020-01-16 10:29:21'),
(8, 'maria', '$2y$12$86kXcCAN/GHX9Q0i9SSoXOGDhZDtl66rGoJdyDZfnuaexGX/pYJc.', 'Maria', 'Fernandez', 'sdf', '2020-01-16 10:35:46');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT voor een tabel `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
